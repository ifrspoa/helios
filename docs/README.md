## Sobre

O Sistema de Votação Online disponível no endereço https://vote.canoas.ifrs.edu.br utiliza o sistema de código aberto [Helios](https://heliosvoting.org/) desenvolvido por Ben Adida do Instituto de Tecnologia de Massachusetts (EUA) (MIT, na sigla em inglês) e adaptado para o público brasileiro pelo Instituto Federal de Santa Catarina.

O sistema permite a realização de eleições através da Internet, é auditável ([End-to-end voter verifiable – E2E](https://en.wikipedia.org/wiki/End-to-end_auditable_voting_systems)) e faz uso de criptografia homomórfica de forma que é possível computar o resultado final de uma eleição sem que seja necessário ter acesso ao voto em texto claro (descriptografar o voto) individual de cada eleitor.

O Helios permite que:

* O eleitor verifique se seu voto foi depositado corretamente
* Todos os votos depositados na urna sejam exibidos publicamente em sua forma criptografada
* Qualquer um possa verificar que os votos depositados na urna foram corretamente apurados

O Helios não permite que:

* A escolha de um eleitor (seu voto) seja revelada, mesmo que este eleitor queira revelar (p.e. apresentando um recibo de votação)
* O voto de um eleitor seja adulterado ou excluído
* O mantenedor do banco de dados, normalmente um servidor de T.I. e gestor do sistema com propriedades administrativas tenha acesso ao voto em texto claro

## Auditoria

Para fins de auditoria o código fonte do sistema utilizado pelo IFRS - *Campus* Canoas esta disponível em:

https://gitlab.com/ifrscanoas/comissao-eleitoral/helios/-/releases

## Referências

Algumas das informações utilizadas neste site de ajuda foram copiadas das seguintes instituições:

* IFSC
* UFSC
* IFSP


