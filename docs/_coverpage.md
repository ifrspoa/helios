# Sistema de Votação Online

> Desenvolvido pelo MIT (EUA) e adaptado para o público brasileiro pelo IFSC

- [Saiba mais ...](/#sobre)

[Manual do Eleitor](eleitor.md)
[Manual do Gestor](gestor.md)

![color](#f0f0f0)
