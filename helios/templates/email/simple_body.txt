<!doctype html>
<html lang="pt-br">
<title>{{default_from_name|safe|linebreaksbr}}</title>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>{{voter.name}},</p>

<p>{{custom_message|safe|linebreaksbr}}

<p>Para votar nessa eleição, clique no link: {{election_vote_url}}</p>
<br>
{% ifequal voter.voter_type "password" %}
<p><strong>Seu ID de eleitor:</strong> {{voter.voter_login_id}}
<br><strong>Sua senha para votar nessa eleição:</strong> {{voter.voter_password}}</p>
{% else %}
<p>
Autentique-se com sua conta de usuário {{voter.voter_type}}.
</p>
{% endifequal %}
<br><br>
--
<br>
<font style="" size="1" face="verdana, sans-serif"><b style="">Instituto Federal de Santa Catarina - Reitoria</b>
</font>
<br>
<u><a href="https://ifrs.edu.br/canoas/" target="_blank" style=""><font style="" face="verdana, sans-serif">https://ifrs.edu.br/canoas/</font></a></u>
</body>
</html>
