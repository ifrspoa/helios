#!/bin/bash
set -ex
# Comandos e parâmetros
# -not -path = ignora as pastas e arquivos .git/ docker/ locale/ ./SHA256SUMS.txt ./BUILD.txt 
# sort = ordena os arquivos
# LC_ALL=C = configura a localidade pra ordenação não ficar diferente em ambientes diferentes
# xargs -0 sha256sum = gera hash sha256sum de cada arquivo
# tee = recebe o resultado do comando anterior e armazena em arquivo
#
echo 'Gerando hash sha256 dos arquivos e salvando no arquivo SHA256SUMS.txt'
find . -type f -not -path "./.git/*" -not -path "./docker/*" -not -path "./locale/*" -not -path "./SHA256SUMS.txt" -not -path "./BUILD.txt" -print0 | LC_ALL=C sort -z | xargs -0 sha256sum | tee SHA256SUMS.txt
echo 'Gerando hash do arquivo gerado mostrando apenas na tela'
sha256sum SHA256SUMS.txt